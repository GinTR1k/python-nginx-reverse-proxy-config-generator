# Python Nginx reverse proxy config generator
The script helps to generate a reverse proxy for Nginx Server.

## Usage:
Process server_name, destination_domain and SSL path

## Arguments:
* `-h, --help` - show this help message and exit
* `--server_name SERVER_NAME [SERVER_NAME ...]` - Nginx server name.
  You can provide aliases for proxy separated with space. Required.
* `--destination_url DESTINATION_URL` - What to proxy. Required.
* `--resolver RESOLVER` - DNS resolver. Default: 1.1.1.1
* `--ssl_certificate_path SSL_CERTIFICATE_PATH` - Required
* `--ssl_certificate_key_path SSL_CERTIFICATE_KEY_PATH` - Required
* `--with_http_redirect WITH_HTTP_REDIRECT` - Generate redirect HTTP to HTTPS config.
  Pass `true`. Default: False

## Example:
To access site `https://intelius.com/` with proxy on `https://intelius-com.example.com` you should to write this:
```shell
python3 app.py \
    --server_name intelius-com.example.com \
    --destination_url intelius.com \
    --resolver 8.8.8.8 \
    --ssl_certificate_path /etc/letsencrypt/live/example.com/fullchain.pem \
    --ssl_certificate_key_path /etc/letsencrypt/live/example.com/privkey.pem
```

You'll get:
```
server {
    listen 443;
    listen [::]:443;
    server_name intelius-com.example.com;

    ssl_certificate /etc/letsencrypt/live/example.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/example.com/privkey.pem;

    location / {
        resolver 8.8.8.8;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host intelius.com;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header X-Forwarded-For $remote_addr;

        proxy_pass https://intelius.com/;
    }
}
```

To access site `http://core.telegram.org` (look, it's unsecure HTTP!)
with proxy on `https://core-telegram-org.example.com` you should to write this:
```shell
python3 app.py \
    --server_name core-telegram-org.example.com \
    --destination_url http://core.telegram.org/ \
    --ssl_certificate_path /etc/letsencrypt/live/example.com/fullchain.pem \
    --ssl_certificate_key_path /etc/letsencrypt/live/example.com/privkey.pem \
    --with_http_redirect true
```

You'll get:
```
server {
    listen 80;
    listen [::]:80;
    server_name core-telegram-org.example.com;

    access_log off;

    return 301 https://$host$request_uri;
}

server {
    listen 443;
    listen [::]:443;
    server_name core-telegram-org.example.com;

    ssl_certificate /etc/letsencrypt/live/example.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/example.com/privkey.pem;

    location / {
        resolver 1.1.1.1;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host core.telegram.org;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header X-Forwarded-For $remote_addr;

        proxy_pass http://core.telegram.org/;
    }
}
```

**Example with creating config file:**
```shell
python3 app.py \
    --server_name intelius-com.example.com \
    --destination_url intelius.com \
    --resolver 8.8.8.8 \
    --ssl_certificate_path /etc/letsencrypt/live/example.com/fullchain.pem \
    --ssl_certificate_key_path /etc/letsencrypt/live/example.com/privkey.pem \
> intelius-com.example.com.conf
```

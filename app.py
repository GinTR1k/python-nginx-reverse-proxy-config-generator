from argparse import ArgumentParser
from typing import List
from urllib.parse import urlparse


class ConfigGenerator:
    def generate(self, server_name: List[str], destination_url: str,
                 ssl_certificate_path: str,
                 ssl_certificate_key_path: str,
                 resolver: str, with_http_redirect: bool):
        parsed_destination_url = urlparse(destination_url)

        header_host = self._get_header_host(parsed_destination_url)
        proxy_pass = self._get_proxy_pass(parsed_destination_url,
                                          destination_url)

        return (
            self._get_default_config().format(
                server_name=' '.join(server_name),
                header_host=header_host,
                resolver=resolver,
                proxy_pass=proxy_pass,
                ssl_certificate_path=ssl_certificate_path,
                ssl_certificate_key_path=
                    ssl_certificate_key_path,
            ) + '' + self._get_default_redirect_config()
                .format(server_name=' '.join(server_name))
                if with_http_redirect else ''
        )

    @staticmethod
    def _get_header_host(parsed_destination_url) -> str:
        return (
            parsed_destination_url.path,
            parsed_destination_url.netloc,
        )[bool(parsed_destination_url.netloc)]

    @staticmethod
    def _get_proxy_pass(parsed_destination_url,
                        destination_url: str) -> str:
        return (
            f'https://{parsed_destination_url.path}/',
            destination_url,
        )[bool(parsed_destination_url.netloc)]

    @staticmethod
    def _get_default_config() -> str:
        return """server {{
    listen 443;
    listen [::]:443;
    server_name {server_name};

    ssl_certificate {ssl_certificate_path};
    ssl_certificate_key {ssl_certificate_key_path};

    location / {{
        resolver {resolver};
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host {header_host};
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header X-Forwarded-For $remote_addr;

        proxy_pass {proxy_pass};
    }}
}}

"""

    @staticmethod
    def _get_default_redirect_config() -> str:
        return """server {{
    listen 80;
    listen [::]:80;
    server_name {server_name};

    access_log off;

    return 301 https://$host$request_uri;
}}
"""


if __name__ == '__main__':
    arg_parser = ArgumentParser(
        'Process server_name, destination_domain and SSL path')
    arg_parser.add_argument(
        '--server_name', type=str, nargs='+', required=True,
        help='Nginx server name. You can provide aliases for proxy '
        'separated with space')
    arg_parser.add_argument(
        '--destination_url', required=True, help='What to proxy')
    arg_parser.add_argument('--resolver', default='1.1.1.1', help=
        'DNS resolver. Default: 1.1.1.1')
    arg_parser.add_argument('--ssl_certificate_path', required=True)
    arg_parser.add_argument(
        '--ssl_certificate_key_path', required=True)
    arg_parser.add_argument(
        '--with_http_redirect', default=False, type=bool, help=
        'Generate redirect HTTP to HTTPS config. Pass `true`.'
        'Default: False')
    args = arg_parser.parse_args()

    config = ConfigGenerator().generate(
        server_name=args.server_name,
        destination_url=args.destination_url,
        resolver=args.resolver,
        ssl_certificate_path=args.ssl_certificate_path,
        ssl_certificate_key_path=args.ssl_certificate_key_path,
        with_http_redirect=args.with_http_redirect)
    print(config)
